# Experiemto T + 24
import neurolab as nl
import numpy as np

def run( neuronasCEntrada, nuronasCOculta, algoritmoEntrenamiento, ventana, archivo ):

    input = np.array( ventana )

    input = input.reshape(1, neuronasCEntrada )

    net = nl.load('redesExperimentos/MejorN_1_'+str(archivo)+'.net')

    out = net.sim(input)

    return out[0][0]