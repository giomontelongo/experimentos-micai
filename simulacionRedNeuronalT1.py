# Experimentos de  T + 1
import neurolab as nl
import numpy as np
import Helper

def run( neuronasCEntrada, neuronasCOculta, algoritmoEntrenamiento, numeroPasosPronostico, nb_archivo, archivo, numeroTraining ):

    datos = archivo

    datosEntrenamiento = datos[numeroTraining:]

    inp, tar = Helper.getDataFormatted(datosEntrenamiento, neuronasCEntrada, numeroPasosPronostico)

    net = nl.load("redesMejorT+1/NE_5_NO_2_Ep_3000_MejorN_"+str(numeroPasosPronostico)+"_"+str(nb_archivo)+".net")

    out = net.sim(inp)

    return out, tar