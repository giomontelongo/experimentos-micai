# Proceso de entrenamiento de red neuronal entreando 3 veces obteniendo el mejor error
import Helper
import redNeuronal
import neurolab as nl
import numpy as np

directorio = 'datosVientoPruebas/'

numeroMuestra =  1000
numeroValidacion = 240
numeroTraining = numeroMuestra - numeroValidacion

# [nombreArchivo, nE, nO, aE, numeroPasosPronostico]
datosArchivos = np.array([  
                        # ['wind_aristeomercado_10m_muestra',5,2,4,1],
                        # ['wind_aristeomercado_10m_muestra',46,1,5,2],
                        # ['wind_aristeomercado_10m_muestra',33,1,6,3],
                        # ['wind_aristeomercado_10m_muestra',41,1,6,4],
                        # ['wind_aristeomercado_10m_muestra',37,2,4,5]
                        ['wind_aristeomercado_10m_muestra',37,35,4,5]
                        # ['wind_aristeomercado_10m_muestra',33,1,6,6]
                        # ['wind_aristeomercado_10m_muestra',40,17,6,7],
                        # ['wind_aristeomercado_10m_muestra',35,17,6,8],
                        # ['wind_aristeomercado_10m_muestra',32,12,7,9],
                        # ['wind_aristeomercado_10m_muestra',33,1,4,10],

                        # ['wind_aristeomercado_10m_muestra',36,9,6,11],
                        # ['wind_aristeomercado_10m_muestra',45,6,6,12],
                        # ['wind_aristeomercado_10m_muestra',46,4,6,13],
                        # ['wind_aristeomercado_10m_muestra',41,7,1,14],
                        # ['wind_aristeomercado_10m_muestra',49,1,5,15],
                        # ['wind_aristeomercado_10m_muestra',59,30,7,16],
                        # ['wind_aristeomercado_10m_muestra',37,51,6,17],
                        # ['wind_aristeomercado_10m_muestra',38,21,6,18],
                        # ['wind_aristeomercado_10m_muestra',34,17,6,19],
                        # ['wind_aristeomercado_10m_muestra',16,2,6,20],
                        # ['wind_aristeomercado_10m_muestra',18,5,6,21],
                        # ['wind_aristeomercado_10m_muestra',33,1,7,22],
                        # ['wind_aristeomercado_10m_muestra',34,2,4,23],
                        # ['wind_aristeomercado_10m_muestra',53,17,6,24]
                    ])

# epochs = [5000,7000,10000,20000]
epochs = [100000]
i = 0

# for i in range (0, len(datosArchivos)):
for j in range (0, len(epochs) ):

    nb_archivo = datosArchivos[i][0]
    rutaArchivo = directorio + nb_archivo+'.csv' 
    numeroPasosPronostico = int( datosArchivos[i][4] )
    epocas = epochs[j]

    # print "epocas: ", epocas

    archivo = Helper.obtenerArchivo( rutaArchivo, numeroMuestra)

    # print archivo

    arrayError  = []
    arrayNet    = []

    for j in range(0,3):

        # (nE,nO,aE,numeroPasosPronostico, archivo, numeroTrainig)
        error, net = redNeuronal.run( int( datosArchivos[i][1] ) , int ( datosArchivos[i][2]), int(datosArchivos[i][3]), epocas, numeroPasosPronostico, archivo, numeroTraining)

        print "Error ", (j+1)," : ", error

        arrayError.append(error)
        arrayNet.append(net)

    mejorError = min(arrayError)

    print "Numero de epocas: ", epocas
    print "Mejor Error: ", mejorError

    posicionMejorNet = np.argmin(arrayError)
    mejorNet =  arrayNet[posicionMejorNet]

    mejorNet.save(('redesT+5.1/' + str(epocas) + '_MejorN_' + str(numeroPasosPronostico) + '_'+ str(nb_archivo) +'.net'))

    # # Se simula el t + 1
    # ejecutaSimulacionRedNeuronalT1.run(datosArchivos[i][1], datosArchivos[i][2], datosArchivos[i][3], nb_archivo, archivo)

    # # Se simula el t + 24
    # ejecutaSimulacionRedNeuronal.run(datosArchivos[i][1], datosArchivos[i][2], datosArchivos[i][3], nb_archivo, archivo, numeroTraining)
