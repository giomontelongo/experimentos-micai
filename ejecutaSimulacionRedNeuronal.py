# 24 dias
import simulacionRedNeuronal
import csv
import matplotlib.pyplot as plt
import numpy as np
import Helper
import math

directorio = 'datosVientoPruebas/'
numeroPasosPronostico = 1
t = 24

datosArchivos = np.array([  
                        ['wind_aristeomercado_10m_muestra',59,26,6]
                    ])
x = 0

nE = int ( datosArchivos[x][1] ) # Neuronas en la capa de entrada
nO = int ( datosArchivos[x][2] ) # Neuronas en la capa oculta
aE = int ( datosArchivos[x][3] )  # Algoritmo de entrenamiento

nb_archivo = datosArchivos[x][0]

rutaArchivo = directorio + nb_archivo+'.csv' 

numeroMuestra =  1500

archivo = Helper.obtenerArchivo( rutaArchivo, numeroMuestra)

numeroMinimo, numeroMaximo = Helper.valoresMinMax(rutaArchivo, numeroMuestra)

print ""
print "numeroMaximo: ", numeroMaximo
print "numeroMinimo: ", numeroMinimo

# Se quitan los -1
archivo = Helper.formatear(archivo)

numeroValidacion = 450
numeroMuestra = len(archivo)
numeroTraining = numeroMuestra - numeroValidacion

datosNecesario = 10 * 24 + nE

datosInicio = len(archivo) - datosNecesario

# Datos Pronosticados
pronosticados = []

for iteracion1 in range(0, 10):

	datosEntrenamiento = archivo[datosInicio - 1:]
	ventana = datosEntrenamiento[:nE]

	for iteracion2 in range(0, 24):

		pronostico = simulacionRedNeuronal.run( nE, nO, aE, ventana, nb_archivo )
		pronosticados.append(pronostico)

		ventana.append(pronostico)
		ventana = ventana[1:]

	datosInicio  = datosInicio + 24

tar = archivo[numeroTraining:]

out = pronosticados

Helper.imprimirResultado(tar, out, numeroMinimo, numeroMaximo, aE, nb_archivo, nE, nO, t)

# Crear archivo .csv
archivoCSV = "pronosticos_t24/pronostico_t24_"+str(nb_archivo)+".csv"

with open(archivoCSV, "wb") as ArchivoNuevoCSV:

    writer = csv.writer(ArchivoNuevoCSV, lineterminator='\n')

    writer.writerow(['Target', 'Output'])

    for i in range(0, len(tar)):

    	tarTemp = str(tar[i])
        outTemp = str(out[i])

        aux1 = tarTemp.replace("[", "")
        tarAdd = aux1.replace("]", "")

        aux2 = outTemp.replace("[", "")
        outAdd = aux2.replace("]", "")

        writer.writerow( (tarAdd, outAdd) )