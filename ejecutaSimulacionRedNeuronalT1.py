# Experimentos de t+1
import simulacionRedNeuronalT1
import csv
import matplotlib.pyplot as plt
import numpy as np
import Helper
import math

directorio = 'datosVientoPruebas/'

numeroMuestra =  1000
numeroValidacion = 240
numeroTraining = numeroMuestra - numeroValidacion

t = 1

datosArchivos = np.array([  
                        ['wind_aristeomercado_10m_muestra',5,2,4,1],
                        # ['wind_aristeomercado_10m_muestra',46,35,5,2],
                        # ['wind_aristeomercado_10m_muestra',33,30,6,3],
                        # ['wind_aristeomercado_10m_muestra',34,21,6,4]
                        # ['wind_aristeomercado_10m_muestra',37,35,4,5],
                        # ['wind_aristeomercado_10m_muestra',33,30,6,6],
                        # ['wind_aristeomercado_10m_muestra',40,33,6,7],





                        #['wind_aristeomercado_10m_muestra',37,30,4,5],



                        # ['wind_aristeomercado_10m_muestra',46,1,5,2],
                        # ['wind_aristeomercado_10m_muestra',33,30,6,3],
                        # ['wind_aristeomercado_10m_muestra',41,1,6,4],
                        #['wind_aristeomercado_10m_muestra',37,30,4,5]
                        # ['wind_aristeomercado_10m_muestra',33,1,6,6],
                        # ['wind_aristeomercado_10m_muestra',40,17,6,7],
                        # ['wind_aristeomercado_10m_muestra',35,17,6,8],
                        # ['wind_aristeomercado_10m_muestra',32,12,7,9],
                        # ['wind_aristeomercado_10m_muestra',33,1,4,10]
                    ])

# Posicion del archivo a simular
x = 0

neuronasCentrada = int ( datosArchivos[x][1] )
neuronasCOculta = int  ( datosArchivos[x][2] )
algoritmoEntrenamiento = int ( datosArchivos[x][3] )

numeroPasosPronostico = int ( datosArchivos[x][4] )

nb_archivo = datosArchivos[x][0]

rutaArchivo = directorio + nb_archivo+'.csv' 

archivo = Helper.obtenerArchivo( rutaArchivo, numeroMuestra)

numeroMinimo, numeroMaximo = Helper.valoresMinMax(rutaArchivo, numeroMuestra)

print ""
print "numeroMaximo: ", numeroMaximo
print "numeroMinimo: ", numeroMinimo

out, tar = simulacionRedNeuronalT1.run( neuronasCentrada, neuronasCOculta, algoritmoEntrenamiento, numeroPasosPronostico, nb_archivo, archivo, numeroTraining )

Helper.imprimirResultado(tar, out, numeroMinimo, numeroMaximo, algoritmoEntrenamiento, nb_archivo, neuronasCentrada, neuronasCOculta, t)

archivoCSV = "pronosticos_t1/1-pronostico_t1_"+str(numeroPasosPronostico)+"_"+str(nb_archivo)+".csv"

with open(archivoCSV, "wb") as ArchivoNuevoCSV:

    writer = csv.writer(ArchivoNuevoCSV, lineterminator='\n')

    writer.writerow(['Target', 'Output'])

    for i in range(0, len(tar)):

    	tarTemp = str(tar[i])
        outTemp = str(out[i])

        aux1 = tarTemp.replace("[", "")
        tarAdd = aux1.replace("]", "")

        aux2 = outTemp.replace("[", "")
        outAdd = aux2.replace("]", "")

        writer.writerow( (tarAdd, outAdd) )