# Proceso de entrenamiento de red neuronal entreando 3 veces obteniendo el mejor error
import Helper
import redNeuronal
import neurolab as nl
import numpy as np

directorio = 'datosVientoPruebas/'

numeroMuestra =  1000
numeroValidacion = 240
numeroTraining = numeroMuestra - numeroValidacion

# [nombreArchivo, nE, nO, aE, numeroPasosPronostico]

# la primera fue con 35 nO y en la 2da con 30 nO de T+5
datosArchivos = np.array([ 
                         # ['wind_aristeomercado_10m_muestra',5,2,4,1],
                        # ['wind_aristeomercado_10m_muestra',46,1,5,2],
                        # ['wind_aristeomercado_10m_muestra',33,1,6,3],
                        # ['wind_aristeomercado_10m_muestra',41,1,6,4],
                        # ['wind_aristeomercado_10m_muestra',37,2,4,5]
                        #['wind_aristeomercado_10m_muestra',37,35,4,5]
                        # ['wind_aristeomercado_10m_muestra',33,1,6,6]
                        # ['wind_aristeomercado_10m_muestra',40,17,6,7],
                        # ['wind_aristeomercado_10m_muestra',35,17,6,8],
                        # ['wind_aristeomercado_10m_muestra',32,12,7,9],
                        # ['wind_aristeomercado_10m_muestra',33,1,4,10],
                        # ['wind_aristeomercado_10m_muestra',36,9,6,11],
                        # ['wind_aristeomercado_10m_muestra',45,6,6,12],
                        # ['wind_aristeomercado_10m_muestra',46,4,6,13],
                        # ['wind_aristeomercado_10m_muestra',41,7,1,14],
                        # ['wind_aristeomercado_10m_muestra',49,1,5,15],
                        # ['wind_aristeomercado_10m_muestra',59,30,7,16],
                        # ['wind_aristeomercado_10m_muestra',37,51,6,17],
                        # ['wind_aristeomercado_10m_muestra',38,21,6,18],
                        # ['wind_aristeomercado_10m_muestra',34,17,6,19],
                        # ['wind_aristeomercado_10m_muestra',16,2,6,20],
                        # ['wind_aristeomercado_10m_muestra',18,5,6,21],
                        # ['wind_aristeomercado_10m_muestra',33,1,7,22],
                        # ['wind_aristeomercado_10m_muestra',34,2,4,23],
                        # ['wind_aristeomercado_10m_muestra',53,17,6,24]




                        ['wind_aristeomercado_10m_muestra',18,5,6,21],
                        # ['wind_aristeomercado_10m_muestra',26,12,6,20]
                    ])

epochs = [5000,7000,10000,20000,50000]
#epochs = [3000]

# i = 0

for i in range (0, len(datosArchivos)):

    for j in range (0, len(epochs) ):

        nb_archivo = datosArchivos[i][0]
        rutaArchivo = directorio + nb_archivo+'.csv' 
        numeroPasosPronostico = int( datosArchivos[i][4] )
        epocas = epochs[j]

        archivo = Helper.obtenerArchivo( rutaArchivo, numeroMuestra)

        arrayError  = []
        arrayNet    = []

        neuronasEntrada = int( datosArchivos[i][1] )
        neuronasOcultas = int ( datosArchivos[i][2])

        for j in range(0,3):

            # (nE,nO,aE,numeroPasosPronostico, archivo, numeroTrainig)
            error, net = redNeuronal.run( neuronasEntrada, neuronasOcultas, int(datosArchivos[i][3]), epocas, numeroPasosPronostico, archivo, numeroTraining)

            # print "Error ", (j+1)," : ", error

            # net.save(('redesPruebasT+' + str(numeroPasosPronostico) + '/' + str(j+1) + '_NE_' + str(neuronasEntrada) + '_NO_' + str(neuronasOcultas) + '_Ep_' + str(epocas) + '_MejorN_' + str(numeroPasosPronostico) + '_'+ str(nb_archivo) +'.net'))

            # print "Red Guardada"

            arrayError.append(error)
            arrayNet.append(net)

        mejorError = min(arrayError)

        print ""
        print "Neuronas de Entrada: ", neuronasEntrada
        print "Neuronas Ocultas: ", neuronasOcultas
        print "Numero de epocas: ", epocas
        print "numeroPasosPronostico: ", numeroPasosPronostico
        print ""
        print "Mejor Error: ", mejorError
        print ""

        posicionMejorNet = np.argmin(arrayError)
        mejorNet =  arrayNet[posicionMejorNet]

        mejorNet.save(('redesMejorT+' + str(numeroPasosPronostico) + '/NE_' + str(neuronasEntrada) + '_NO_' + str(neuronasOcultas) + '_Ep_' + str(epocas) + '_MejorN_' + str(numeroPasosPronostico) + '_'+ str(nb_archivo) +'.net'))